package pl.codementors.helloworld;

import java.util.Scanner;

/**
 * Welcome class.
 * @author psysiu
 */
public class HelloWorld {

    /**
     * Welcome method.
     * @param args Application starting parameters.
     */
    public static void main(String[] args) {
        System.out.println("Hello World!");
        
        byte byteVar = 1;
        short shortVar = 1;
        int intVar = 1;
        long longVar = 1;
        float floatVar = 1;
        double doubleVar = 1;
        boolean booleanVar = true;
        char charVar = 'a';
        
        System.out.println(byteVar);
        System.out.println(shortVar);
        System.out.println(intVar);
        System.out.println(longVar);
        System.out.println(floatVar);
        System.out.println(doubleVar);
        System.out.println(booleanVar);
        System.out.println(charVar);
        
        Scanner inputScanner = new Scanner(System.in);

        System.out.println("Podaj proszę byta");
        byteVar = inputScanner.nextByte();
        System.out.println("podałeś : " + byteVar);

        System.out.println("Podaj proszę shorta");
        shortVar = inputScanner.nextShort();
        System.out.println("podałeś : " + shortVar);

        System.out.println("Podaj proszę inta");
        intVar = inputScanner.nextInt();
        System.out.println("podałeś : " + intVar);

        System.out.println("Podaj proszę longa");
        longVar = inputScanner.nextLong();
        System.out.println("podałeś : " + longVar);

        System.out.println("Podaj proszę floata");
        floatVar = inputScanner.nextFloat();
        System.out.println("podałeś : " + floatVar);

        System.out.println("Podaj proszę doubla");
        doubleVar = inputScanner.nextDouble();
        System.out.println("podałeś : " + doubleVar);

        System.out.println("Podaj proszę booleana");
        booleanVar = inputScanner.nextBoolean();
        System.out.println("podałeś : " + booleanVar);

        /*
        System.out.println("Podaj proszę chara");
        charVar = inputScanner.nextChar();
        System.out.println("podałeś : " + charVar);
        */
    }
}

